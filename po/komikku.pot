# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Komikku package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Komikku 0.21.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-06 00:31+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/info.febvre.Komikku.appdata.xml.in:9
msgid "An online/offline manga reader for GNOME"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:11
msgid ""
"An online/offline manga reader for GNOME developed with the aim of being "
"used with the Librem 5 phone"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:40
msgid "Valéry Febvre"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:51
msgid "[Library] Several bugs fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:58
msgid "[Library] Rounded mangas thumbnails"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:59
msgid "[L10n] German translation update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:60
msgid ""
"Fixed circular import error that prevented Komikku from starting (only on "
"first launch, didn't affect Flatpak)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:67
msgid "[Library] Search: Add filter options (Downloaded, Unread, Recent)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:68
msgid ""
"[Preferences] The window and its subpages can now be closed with Esc key"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:69
msgid "[Servers] Add Dynasty (EN)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:70
msgid "[Servers] Add 'Lupi Team' and 'Tutto Anime Manga' (IT)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:71
msgid "[Servers] Add VizManga (EN): an account is required"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:72
msgid "[Servers] Disable Jaimini's Box (definitely closed)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:73
msgid "[L10n] Add German translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:80
msgid "Add HiDPI display support"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:81
msgid ""
"[Preferences] Use Handy.PreferencesWindow subpages to display Servers "
"Languages and Servers Settings"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:88
msgid "[Library] Press &lt;Enter&gt; opens first manga in search"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:89
msgid "[Library] Disable range selection when in search mode"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:90
msgid ""
"[Library/Chapters/Download manager] Enter selection mode with Right click"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:91
msgid ""
"[Reader] Two new navigation types: mouse scroll, 2-fingers swipe gesture"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:92
msgid "[Reader] New 'Save Page' action"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:93
msgid "[Servers] Fix and improve credentials storage"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:94
msgid "[Servers] Crunchyroll: Fix login"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:95
msgid "[Servers] Re-enable Jaimini's Box"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:96
msgid "[Servers] Manga Eden: Fix downloading of images"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:97
msgid "[Servers] MangaSee: Update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:98
msgid "[Servers] Manga Plus: Add manga 'Status' info"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:99
msgid "[Servers] Manga Plus: Can't get manga info of some manga"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:100
msgid "[Servers] MangaDex: Fix new chapters not appearing"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:101
msgid "[Servers] Webtoon: Fix search"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:102
msgid "[Preferences] Add search"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:103
msgid "Add Turkish translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:110
msgid "[Preferences] New UI"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:111
msgid "[Servers] MangaNelo: Fix images loading"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:112
msgid "[Servers] Disable Jaimini's Box"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:118
msgid "Fix 2 critical bugs (app crashs)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:124
msgid "[Library/Readers] Add support of animated GIFs"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:125
msgid "[Library] Add &lt;Control&gt;+F shortcut: Enter search mode"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:126
msgid ""
"[Library/Manga/Download Manager] Add &lt;Control&gt;+A shortcut: Select all"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:127
msgid ""
"[Library/Manga/Download Manger] Add &lt;Control&gt;+Click and &lt;Shift&gt;"
"+Click shortcuts: Enter selection mode"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:128
msgid ""
"[Download Manager] Add range selection: long press on an item then long "
"press on another to select everything in between"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:129
msgid "[Manga] Add chapters scanlators support (MangaDex only)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:130
msgid ""
"[Servers] Add Izneo (DE, EN and FR) and Yieha (NL): a account is required"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:137
msgid "[Library] Faster chapters list rendering"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:138
msgid ""
"[Library] Faster read/unread state toggling with long chapters selections"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:139
msgid "[Library] Reduce memory used by covers"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:140
msgid "[Servers] MangaSee: Fix manga with chapters grouped by seasons"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:141
msgid "[Servers] Jaimini's Box: Several fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:142
msgid "[Servers] MangaLib: Several fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:149
msgid "[Library] Add search by genre (exact match)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:150
msgid ""
"[Library/Chapters] Add range selection: long press on an item then long "
"press on another to select everything in between"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:151
msgid "[Chapters] Fully cached/read chapters are now marked as \"Downloaded\""
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:152
msgid "[Reader] Improve keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:153
msgid "[Reader] Improve pages transition"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:154
msgid "[Servers] Add Crunchyroll (EN): a Premium account is required"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:155
msgid "[Servers] Scantrad France: Add Most populars"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:156
msgid "Improve manga update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:157
msgid "New icon"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:164
msgid "[Reader] Add 'Original Size' scaling"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:165
msgid "[Reader] Several fixes in keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:166
msgid "[Settings] Library: Add new option 'Auto Download of New Chapters'"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:167
msgid ""
"[Settings] Library: Add new option 'Long Strip Detection' (MangaDex, "
"MangaNelo, Mangakawaii, JapScan and Union Mangás)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:168
msgid "[Servers] JapScan: Fix search and images loading"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:169
msgid "[Servers] MangaLib: Fix images loading"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:170
msgid "[Servers] Submanga: Update of domain"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:176
msgid "[Reader] Fix crash with corrupt or empty images"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:177
msgid "[Settings] Add servers settings"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:178
msgid "[Servers] Add Union Mangás (PT)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:179
msgid "[Servers] Add MangaLib and HentaiLib (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:180
msgid ""
"[Servers] Add Edelgarde Scans, Hunlight Scans, One Shot Scans, Reaper Scans, "
"The Nonames Scans, Zero Scans (EN)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:181
msgid "[Servers] Add Leviatan Scans (EN, ES)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:182
msgid "[Servers] Hatigarm Scans: new server version"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:183
msgid "[Servers] Scantrad France: Fix"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:184
msgid "[Servers] MangaDex: Added ability to log in"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:185
msgid "[Servers] Read Manga: Fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:186
#: ../data/info.febvre.Komikku.appdata.xml.in:192
msgid "Improved manga update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:191
msgid "[Servers] Mangakawaii: Fixed manga cover"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:197
msgid "[Manga] Fixed the unread action of chapters"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:198
msgid "[Reader] Cursor hiding during keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:199
msgid "[Reader] Arrow scrolling in every reading directions"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:200
msgid "[Reader] Added white borders crop"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:201
msgid "[Servers] Added Kirei Cake (EN)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:202
msgid "[Servers] Added Read Manga, Mint Manga and Self Manga (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:203
msgid "[Servers] Updated Mangakawaii domain"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:204
msgid "[Servers] Fixed JapScan search (using DuckDuckGo Lite)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:205
msgid "Added back navigation with Escape key"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:206
msgid ""
"Added abilility to add manga by ID using keyword \"id:&lt;id&gt;\" (useful "
"with MangaDex)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:207
msgid "Various bugs fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:212
msgid "Added the Dutch translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:217
msgid "Added Desu server (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:218
msgid "Added the Russian translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:219
msgid "Updated the Brazilian Portuguese translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:220
msgid "Fixed a bug in manga updater"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:225
msgid "Fixe a bug in Download Manager"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:230
msgid "Added a Download Manager"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:231
msgid "[Manga] Improved chapters download"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:232
msgid ""
"[Reader] Vertical reading direction: Added scrolling with UP and DOWN keys"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:233
msgid "[Reader] Added a Retry button when a image load failed"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:234
msgid "[Servers] Removed Manga Rock (server has closed)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:235
msgid "[Servers] Minor fix in xkcd"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:236
msgid "[Servers] MangaDex: Added 15 languages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:241
msgid "[Library] Add an unread badge"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:242
msgid "[Library/Manga] Added \"Select All\" in selection mode"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:243
msgid ""
"[Library/Manga] Now leaves selection mode when no mangas/chapters are "
"selected"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:244
msgid "[Manga] Improved chapters list rendering"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:245
msgid ""
"[Manga] Improved chapters download: add a progress bar and a \"Stop\" button"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:246
msgid "[Settings] General: New option to disable desktop notifications"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:247
msgid "[Servers] Minor fixes in xkcd"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:248
msgid "[Servers] Add MangaDex (EN, ES, FR)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:253
msgid "Jaimini's Box server: Fixed mangas with an adult alert"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:258
msgid "Added MANGA Plus server (EN, ES)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:259
msgid "Added Jaimini's Box"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:264
msgid "Fixed 'Night Light' preference"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:265
msgid "Fixed JapScan server search"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:270
msgid ""
"Pager focus is now correctly restored when menu is closed (useful for keypad "
"navigation)."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:271
msgid "Happy new year to everyone."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:276
msgid "Added 'Vertical' reading direction"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:277
msgid "Added 'Night Light' preference"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:282
msgid "Fixed Manganelo and WEBTOON servers"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:287
msgid "Ninemanga: Fixed missing chapters issue (EN, BR, DE, ES, IT)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:288
msgid "Pepper&amp;Carrot: Fixed chapters update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:289
msgid "Pepper&amp;Carrot: Added missing Cover and Credits pages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:294
msgid "New preference: Servers languages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:295
msgid "Bug fixes in Scantrad France and DB Multiverse"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:300
msgid "A bug fix in change of reading direction"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:301
msgid "A bug fix in Pepper&amp;Carrot"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:306
msgid "Added Pepper&amp;Carrot server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:307
msgid ""
"A free(libre) and open-source webcomic supported directly by its patrons to "
"change the comic book industry!"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:312
msgid "New preference: Automatically update library at startup"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:313
msgid "Fix in Mangakawaii server (Cloudflare problem)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:318
msgid "Fixes in Mangakawaii server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:323
msgid "Added Dragon Ball Multiverse (DBM) server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:324
msgid "Fixes in Japscan server (search still broken)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:329
msgid "New servers: NineManga Russian, Webtoon Indonesia and Webtoon Thai"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:330
msgid ""
"For all servers that allow it, most popular mangas are now offered as "
"starting search results."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:335
msgid "Improve speed of dates parsing"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:336
msgid "Fixed order of manga chapters for Scantrad France and Central de Mangás"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:341
msgid "Fix keyboard navigation in reader"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:346
msgid "Add a new server: xkcd (English)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:351
msgid "Add the Portuguese (Brazil) translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:352
msgid "Add a new server: Central de Mangas (Portuguese)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:357
msgid "Bug fix: Change the location of the data storage"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:362
msgid "First release"
msgstr ""

#: ../data/info.febvre.Komikku.desktop.in:3
msgid "@prettyname@"
msgstr ""

#: ../data/info.febvre.Komikku.desktop.in:7
msgid "@appid@"
msgstr ""

#: ../data/info.febvre.Komikku.desktop.in:13
msgid "manga;reader;viewer;comic;webtoon;scan;offline;"
msgstr ""

#: ../data/ui/about_dialog.ui.in:11
msgid ""
"An online/offline manga reader.\n"
"\n"
"Never forget, you can support the authors\n"
"by buying the official comics when they are\n"
"available in your region/language."
msgstr ""

#: ../data/ui/about_dialog.ui.in:17
msgid "Learn more about Komikku"
msgstr ""

#: ../data/ui/add_dialog.ui:46
msgid "Select a server"
msgstr ""

#: ../data/ui/add_dialog.ui:301 ../data/ui/application_window.ui:420
msgid "Authors"
msgstr ""

#: ../data/ui/add_dialog.ui:329 ../data/ui/application_window.ui:448
msgid "Genres"
msgstr ""

#: ../data/ui/add_dialog.ui:357 ../data/ui/application_window.ui:476
msgid "Status"
msgstr ""

#: ../data/ui/add_dialog.ui:385 ../data/ui/application_window.ui:504
msgid "Server"
msgstr ""

#: ../data/ui/add_dialog.ui:413 ../data/ui/application_window.ui:533
msgid "Synopsis"
msgstr ""

#: ../data/ui/add_dialog.ui:444 ../data/ui/application_window.ui:618
msgid "Scanlators"
msgstr ""

#: ../data/ui/application_window.ui:563
msgid "Last update"
msgstr ""

#: ../data/ui/application_window.ui:638
msgid "Info"
msgstr ""

#: ../data/ui/application_window.ui:669
msgid "Chapters"
msgstr ""

#. The subtitle of the umbrella sentence in the first start screen. This is a sentence which gives the user a starting point what he can do if he opens the application for the first time.
#: ../data/ui/application_window.ui:825
msgid "An online/offline manga reader"
msgstr ""

#: ../data/ui/download_manager_dialog.ui:32 ../data/ui/menu/main.xml:11
msgid "Download Manager"
msgstr ""

#: ../data/ui/download_manager_dialog.ui:201
msgid "No downloads"
msgstr ""

#: ../data/ui/menu/card.xml:7
#: ../data/ui/menu/download_manager_selection_mode.xml:7
#: ../data/ui/menu/library_selection_mode.xml:11
msgid "Delete"
msgstr ""

#: ../data/ui/menu/card.xml:11 ../data/ui/menu/library_selection_mode.xml:7
msgid "Update"
msgstr ""

#: ../data/ui/menu/card.xml:18
msgid "Order of Chapters"
msgstr ""

#: ../data/ui/menu/card.xml:23
msgid "By Chapter Number (9-0)"
msgstr ""

#: ../data/ui/menu/card.xml:28
msgid "By Chapter Number (0-9)"
msgstr ""

#: ../data/ui/menu/card.xml:38
msgid "Open in Browser"
msgstr ""

#: ../data/ui/menu/card_selection_mode.xml:15
#: ../data/ui/menu/library_selection_mode.xml:22
msgid "Mark as Read"
msgstr ""

#: ../data/ui/menu/card_selection_mode.xml:19
#: ../data/ui/menu/library_selection_mode.xml:26
msgid "Mark as Unread"
msgstr ""

#: ../data/ui/menu/card_selection_mode.xml:26
#: ../data/ui/menu/library_selection_mode.xml:33
msgid "Select All"
msgstr ""

#: ../data/ui/menu/download_manager.xml:7
msgid "Delete All"
msgstr ""

#: ../data/ui/menu/library_search.xml:11
msgid "Unread"
msgstr ""

#: ../data/ui/menu/library_search.xml:15
msgid "Recents"
msgstr ""

#: ../data/ui/menu/main.xml:7
msgid "Update Library"
msgstr ""

#: ../data/ui/menu/main.xml:17
msgid "Preferences"
msgstr ""

#: ../data/ui/menu/main.xml:21
msgid "Keyboard Shortcuts"
msgstr ""

#: ../data/ui/menu/main.xml:25
msgid "About Komikku"
msgstr ""

#: ../data/ui/menu/reader.xml:7
msgid "Reading direction"
msgstr ""

#: ../data/ui/menu/reader.xml:27
msgid "Webtoon ↓"
msgstr ""

#: ../data/ui/menu/reader.xml:35
msgid "Type of Scaling"
msgstr ""

#: ../data/ui/menu/reader.xml:63 ../data/ui/preferences_window.ui:233
msgid "Background Color"
msgstr ""

#: ../data/ui/menu/reader.xml:81
msgid "Crop Borders"
msgstr ""

#: ../data/ui/preferences_window.ui:18
msgid "General"
msgstr ""

#: ../data/ui/preferences_window.ui:27
msgid "Dark Theme"
msgstr ""

#: ../data/ui/preferences_window.ui:29
msgid "Use dark GTK theme"
msgstr ""

#: ../data/ui/preferences_window.ui:44
msgid "Night Light"
msgstr ""

#: ../data/ui/preferences_window.ui:46
msgid "Automatically enable dark theme at night"
msgstr ""

#: ../data/ui/preferences_window.ui:61
msgid "Desktop Notifications"
msgstr ""

#: ../data/ui/preferences_window.ui:63
msgid "Use desktop notifications for downloads and library updates"
msgstr ""

#: ../data/ui/preferences_window.ui:92
msgid "Update at Startup"
msgstr ""

#: ../data/ui/preferences_window.ui:94
msgid "Automatically update library at startup"
msgstr ""

#: ../data/ui/preferences_window.ui:110
msgid "Auto Download of New Chapters"
msgstr ""

#: ../data/ui/preferences_window.ui:112
msgid "Automatically download new chapters"
msgstr ""

#: ../data/ui/preferences_window.ui:130
msgid "Servers"
msgstr ""

#: ../data/ui/preferences_window.ui:135 ../data/ui/preferences_window.ui:318
msgid "Servers Languages"
msgstr ""

#: ../data/ui/preferences_window.ui:136
msgid "Restrict servers to selected languages"
msgstr ""

#: ../data/ui/preferences_window.ui:150 ../data/ui/preferences_window.ui:370
msgid "Servers Settings"
msgstr ""

#: ../data/ui/preferences_window.ui:151
msgid "Enable/disable and configure servers"
msgstr ""

#: ../data/ui/preferences_window.ui:165
msgid "Long Strip Detection"
msgstr ""

#: ../data/ui/preferences_window.ui:167
msgid "Automatically detect long vertical strip when possible"
msgstr ""

#: ../data/ui/preferences_window.ui:185
msgid "Whether to enable servers with NSFW only content"
msgstr ""

#: ../data/ui/preferences_window.ui:209
msgid "Reader"
msgstr ""

#: ../data/ui/preferences_window.ui:218
msgid "Reading Direction"
msgstr ""

#: ../data/ui/preferences_window.ui:225
msgid "Scaling"
msgstr ""

#: ../data/ui/preferences_window.ui:226
msgid "Type of scaling to adapt image"
msgstr ""

#: ../data/ui/preferences_window.ui:240
msgid "Borders Crop"
msgstr ""

#: ../data/ui/preferences_window.ui:242
msgid "Crop white borders of images"
msgstr ""

#: ../data/ui/preferences_window.ui:258
msgid "Fullscreen"
msgstr ""

#: ../data/ui/preferences_window.ui:260
msgid "Automatically enter fullscreen mode"
msgstr ""

#: ../data/ui/preferences_window.ui:281
msgid "Advanced"
msgstr ""

#: ../data/ui/preferences_window.ui:286
msgid "Credentials storage"
msgstr ""

#: ../data/ui/preferences_window.ui:291
msgid "Allow plaintext storage as fallback"
msgstr ""

#: ../data/ui/preferences_window.ui:293
msgid "Used when no keyring backends are found"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:14
msgctxt "Shortcut window description"
msgid "Application"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:18
msgctxt "Shortcut window description"
msgid "Open preferences"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:25
msgctxt "Shortcut window description"
msgid "Toggle fullscreen mode"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:32
msgctxt "Shortcut window description"
msgid "Keyboard Shortcuts"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:41
msgctxt "Shortcut window description"
msgid "Library"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:45
msgctxt "Shortcut window description"
msgid "Add manga"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:52
msgctxt "Shortcut window description"
msgid "Select all"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:59
msgctxt "Shortcut window description"
msgid "Search"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:68
msgctxt "Shortcut window description"
msgid "Manga Card"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:72
msgctxt "Shortcut window description"
msgid "Select all chapters"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:94
msgctxt "Shortcut window description"
msgid "Download Manager"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:98
msgctxt "Shortcut window description"
msgid "Select all downloads"
msgstr ""

#: ../komikku/add_dialog.py:262
#, python-brace-format
msgid "{0} manga added"
msgstr ""

#: ../komikku/add_dialog.py:365
msgid "MOST POPULARS"
msgstr ""

#: ../komikku/add_dialog.py:398
msgid "Oops, search failed. Please try again."
msgstr ""

#: ../komikku/add_dialog.py:400
msgid "No results"
msgstr ""

#: ../komikku/add_dialog.py:488
msgid "Oops, failed to retrieve manga's information."
msgstr ""

#: ../komikku/add_dialog.py:512
#, python-brace-format
msgid "Search in {0}…"
msgstr ""

#: ../komikku/application.py:74 ../komikku/application.py:78
msgid "Komikku"
msgstr ""

#: ../komikku/application.py:289 ../komikku/reader/__init__.py:207
msgid "Cancel"
msgstr ""

#: ../komikku/application.py:289
msgid "Yes"
msgstr ""

#: ../komikku/application.py:351
msgid "Contributors: Code, Patches, Debugging:"
msgstr ""

#: ../komikku/application.py:378
msgid "Are you sure you want to quit?"
msgstr ""

#: ../komikku/application.py:381
msgid "Some chapters are currently being downloaded."
msgstr ""

#: ../komikku/application.py:383
msgid "Some mangas are currently being updated."
msgstr ""

#: ../komikku/application.py:386
msgid "Quit?"
msgstr ""

#: ../komikku/card.py:86
#, python-brace-format
msgid ""
"NOTICE\n"
"{0} server is not longer supported.\n"
"Please switch to another server."
msgstr ""

#: ../komikku/card.py:132 ../komikku/library.py:205
msgid "Delete?"
msgstr ""

#: ../komikku/card.py:133
msgid "Are you sure you want to delete this manga?"
msgstr ""

#: ../komikku/card.py:469
msgid "New"
msgstr ""

#: ../komikku/card.py:485
msgid "%m/%d/%Y"
msgstr ""

#: ../komikku/card.py:575
msgid "Reset"
msgstr ""

#: ../komikku/card.py:577
msgid "Download"
msgstr ""

#: ../komikku/card.py:579
msgid "Mark as read"
msgstr ""

#: ../komikku/card.py:581
msgid "Mark as unread"
msgstr ""

#: ../komikku/card.py:754
#, python-brace-format
msgid "Disk space used: {0}"
msgstr ""

#: ../komikku/downloader.py:191
msgid "Download completed"
msgstr ""

#: ../komikku/downloader.py:192 ../komikku/downloader.py:221
#, python-brace-format
msgid "[{0}] Chapter {1}"
msgstr ""

#: ../komikku/downloader.py:215
#, python-brace-format
msgid "{0}/{1} pages downloaded"
msgstr ""

#: ../komikku/downloader.py:217
msgid "error"
msgstr ""

#: ../komikku/library.py:206
msgid "Are you sure you want to delete selected mangas?"
msgstr ""

#: ../komikku/library.py:398
msgid "Library"
msgstr ""

#: ../komikku/models/database.py:251
msgid "Complete"
msgstr ""

#: ../komikku/models/database.py:252
msgid "Ongoing"
msgstr ""

#: ../komikku/models/database.py:253
msgid "Suspended"
msgstr ""

#: ../komikku/models/database.py:254
msgid "Hiatus"
msgstr ""

#: ../komikku/models/database.py:742
msgid "Download pending"
msgstr ""

#: ../komikku/models/database.py:743
msgid "Downloaded"
msgstr ""

#: ../komikku/models/database.py:744
msgid "Downloading"
msgstr ""

#: ../komikku/models/database.py:745
msgid "Download error"
msgstr ""

#: ../komikku/preferences_window.py:110
msgid "Right to Left ←"
msgstr ""

#: ../komikku/preferences_window.py:111
msgid "Left to Right →"
msgstr ""

#: ../komikku/preferences_window.py:112
msgid "Vertical ↓"
msgstr ""

#: ../komikku/preferences_window.py:120
msgid "Adapt to Screen"
msgstr ""

#: ../komikku/preferences_window.py:121
msgid "Adapt to Width"
msgstr ""

#: ../komikku/preferences_window.py:122
msgid "Adapt to Height"
msgstr ""

#: ../komikku/preferences_window.py:123
msgid "Original Size"
msgstr ""

#: ../komikku/preferences_window.py:131
msgid "White"
msgstr ""

#: ../komikku/preferences_window.py:132
msgid "Black"
msgstr ""

#: ../komikku/preferences_window.py:366
msgid "User Account"
msgstr ""

#: ../komikku/preferences_window.py:386
msgid "System keyring service is disabled. Credential cannot be saved."
msgstr ""

#: ../komikku/preferences_window.py:391
msgid ""
"No keyring backends were found to store credential. Use plaintext storage as "
"fallback."
msgstr ""

#: ../komikku/preferences_window.py:396
msgid ""
"No keyring backends were found to store credential. Plaintext storage will "
"be used as fallback."
msgstr ""

#: ../komikku/preferences_window.py:399
msgid "Test"
msgstr ""

#: ../komikku/reader/__init__.py:203
msgid "Please choose a file"
msgstr ""

#: ../komikku/reader/__init__.py:209
msgid "Save"
msgstr ""

#: ../komikku/reader/__init__.py:228
msgid ""
"Failed to save page: missing permission to access the XDG pictures directory"
msgstr ""

#: ../komikku/reader/__init__.py:232
#, python-brace-format
msgid "Page successfully saved to {0}"
msgstr ""

#: ../komikku/reader/pager/__init__.py:443
#: ../komikku/reader/pager/__init__.py:472
#: ../komikku/reader/pager/webtoon.py:151
msgid "There is no previous chapter."
msgstr ""

#: ../komikku/reader/pager/__init__.py:445
#: ../komikku/reader/pager/__init__.py:470
#: ../komikku/reader/pager/webtoon.py:149
msgid "It was the last chapter."
msgstr ""

#: ../komikku/reader/pager/__init__.py:499
msgid "This chapter is inaccessible."
msgstr ""

#: ../komikku/reader/pager/page.py:272
msgid "Failed to load image"
msgstr ""

#: ../komikku/reader/pager/page.py:349
msgid "Retry"
msgstr ""

#: ../komikku/servers/dynasty.py:36
msgid "Categories"
msgstr ""

#: ../komikku/servers/dynasty.py:37
msgid "Types of manga to search for"
msgstr ""

#: ../komikku/servers/dynasty.py:40
msgid "Anthology"
msgstr ""

#: ../komikku/servers/dynasty.py:41
msgid "Doujins"
msgstr ""

#: ../komikku/servers/dynasty.py:42
msgid "Issues"
msgstr ""

#: ../komikku/servers/dynasty.py:43
msgid "Series"
msgstr ""

#: ../komikku/servers/dynasty.py:49
msgid "With Tags"
msgstr ""

#: ../komikku/servers/dynasty.py:50
msgid "Tags to search for"
msgstr ""

#: ../komikku/servers/dynasty.py:56
msgid "Without Tags"
msgstr ""

#: ../komikku/servers/dynasty.py:57
msgid "Tags to exclude from search"
msgstr ""

#: ../komikku/servers/readmanhwa.py:44
msgid "NSFW Content"
msgstr ""

#: ../komikku/servers/readmanhwa.py:45
msgid "Whether to show manga containing NSFW content"
msgstr ""

#: ../komikku/updater.py:83
msgid "Library update completed"
msgstr ""

#: ../komikku/updater.py:85
msgid "Update completed"
msgstr ""

#: ../komikku/updater.py:92
msgid "No new chapter found"
msgstr ""

#: ../komikku/updater.py:117
msgid "Oops, update has failed. Please try again."
msgstr ""

#: ../komikku/updater.py:131
msgid "Library update started"
msgstr ""

#: ../komikku/updater.py:133
msgid "Update started"
msgstr ""

#: ../komikku/utils.py:96
msgid "No Internet connection, timeout or server down"
msgstr ""
